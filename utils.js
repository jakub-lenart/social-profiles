const R = require('ramda');
const uuid = require('uuid');

const createEntity = (entityType, profiles) => ({
  id: uuid.v4(),
  entityType,
  profiles
});

const isAssignedToEntity = (profileId, entities) =>
  R.flatten(entities.map(it => it.profiles.filter(profile => profile.id === profileId))).length > 0;

const getLinkedProfiles = (profile, profiles) =>
  R.flatten(
    // TODO: if link is a website check if there is a mutual connection
    // TODO: parse links and profiles urls to link, for example: "https://twitter.com/aaa" and "http://twitter.com/aaa
    profile.linksTo.map(link => profiles.filter(it => it.url === link && it.entityType === profile.entityType))
  );

const handleLinksTo = (profile, profiles, entities, entityProfiles = []) => {
  const linkedProfiles = getLinkedProfiles(profile, profiles);

  return linkedProfiles.filter(it => !isAssignedToEntity(it.id, entities)).length
    ? linkedProfiles.map(it => handleLinksTo(it, profiles, entities, R.append(it, entityProfiles)))
    : R.flatten(entityProfiles);
};

module.exports = {
  createEntity,
  isAssignedToEntity,
  getLinkedProfiles,
  handleLinksTo
};
