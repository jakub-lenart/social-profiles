### Social Profiles

Before first run:

```shell
# Install dependencies
yarn install
``` 

Run script:

```shell
yarn start
```

Params:

```shell
--profiles       # Path to profiles json file
  alias: -p
```

Example: 

```shell
yarn start -p examples/profiles.json
```

Run tests:

```shell
yarn test
```


Future steps: 
- add Typescript support (in progress: `feature/ts-support` branch)
