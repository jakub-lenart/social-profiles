const assert = require('assert');
const expect = require('chai').expect;

const { createEntities, getProfiles } = require('../index');
const { createEntity, isAssignedToEntity, handleLinksTo } = require('../utils');
const profiles = getProfiles('./test/profiles.mock.json');

describe('Utils', () => {
  const entities = createEntities(profiles);

  describe('Check profiles', () => {
    it('should have id', () => profiles.map(it => expect(it.id).to.exist));
  });

  describe('Create entities', () => {
    it('should create entities', () => expect(entities).to.not.be.empty);

    it('check entities number', () => expect(entities.length).equal(3));
  });

  describe('Create single entity', () => {
    let entity = null;

    const profile = profiles[Math.floor(Math.random() * profiles.length)];

    before(() => (entity = createEntity(profile.entityType, [profile])));

    it('should create entity', () => expect(entity).to.not.be.empty);

    it('should include profile', () => expect(entity.profiles[0]).to.equal(profile));

    it('should have proper type', () => expect(entity.entityType).to.equal(profile.entityType));
  });

  describe('Is assigned to entity', () => {
    it('should find profile in entites', () => {
      expect(isAssignedToEntity(profiles[0].id, entities)).to.be.true;
    });
  });

  describe('Handle profile links', () => {
    it('should return linked profiles', () => {
      expect(handleLinksTo(profiles[0], profiles, []).length).to.be.above(0);
    });
  });
});
