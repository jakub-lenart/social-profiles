const fs = require('fs');
const R = require('ramda');
const uuid = require('uuid');
const commandLineArgs = require('command-line-args');

const { createEntity, isAssignedToEntity, handleLinksTo } = require('./utils');

const optionDefinition = [{ name: 'profiles', alias: 'p', type: String }];
const options = commandLineArgs(optionDefinition);

const getProfiles = path => {
  if (path) {
    const profilesFile = fs.readFileSync(path);
    const profiles = JSON.parse(profilesFile);

    // TODO: add profiles validations
    return profiles.map(it => ({ id: uuid.v4(), ...it }));
  } else {
    return [];
  }
};

const createEntities = profiles => {
  let entities = [];

  profiles.map(profile => {
    if (!isAssignedToEntity(profile.id, entities)) {
      const profilesToAdd = [profile, ...R.uniq(R.flatten(handleLinksTo(profile, profiles, entities)))];
      const entity = createEntity(profile.entityType, profilesToAdd);

      entities = R.append(entity, entities);
    }
  });

  return entities;
};

module.exports = {
  createEntities,
  getProfiles
};

createEntities(getProfiles(options.profiles)).map(it => console.log(`Entity ${it.id} \n`, it.profiles));

// TODO: add storage for entities
